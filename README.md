# ffmpeg_for_dummies

Python wrapper for ffmpeg that softens the esoteric filters and command-line arguments.

```terminal
$ python3 ffmpeg_for_dummies.py -h
usage: ffmpeg_for_dummies.py [-h] [-fr FRAMERATE] [-p PATTERN] [-ext FILE_EXTENSION]
                             [-fn FILENAME] [-ts TIME_SCALE] [-w WIDTH] [-ht HEIGHT]
                             [-op OUTPUT_PATH] [-na] [-dr]
                             source

positional arguments:
  source                Path to the source video file or directory containing images.

options:
  -h, --help            show this help message and exit
  -fr FRAMERATE, --framerate FRAMERATE
                        Framerate of encoded video. Default same as input.
  -p PATTERN, --pattern PATTERN
                        Pattern of file names to look for when building a video from frames.
                        Default %04d
  -ext FILE_EXTENSION, --file-extension FILE_EXTENSION
                        File extention of source data, default is png
  -fn FILENAME, --filename FILENAME
                        Overwrite default file name of encoded video with <fn>.mp4. Default
                        is <VIDEO_NAME>_encoded.mp4
  -ts TIME_SCALE, --time-scale TIME_SCALE
                        Multiply the encoded video by this value. Greater than 1 slows the
                        video down, less than 1 speeds it up. Default 1.
  -w WIDTH, --width WIDTH
                        Specify width of the encoded video. If only --width or --height are
                        passed, the other is calculated automatically. Pass a number like
                        0.25x or 2x to resize, or a number to specify directly. Default does
                        not resize.
  -ht HEIGHT, --height HEIGHT
                        Specify the height of the encoded video. Same rules apply as the ones
                        oulined for the width
  -op OUTPUT_PATH, --output-path OUTPUT_PATH
                        Define output path for encoded video. Default is the same direcory as
                        source video
  -na, --no-audio       Drop audio stream. Useful for slowing down high-speed video.
  -dr, --dry-run        Do not encode the video, but print all parameters and final ffmpeg
                        command.
```