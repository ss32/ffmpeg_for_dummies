#!/usr/bin/python3

from os import name
import subprocess
import pathlib
import argparse
import cv2
import os


def make_args():

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "source",
        type=str,
        help="Path to the source video file or directory containing images.",
    )
    parser.add_argument(
        "-fr",
        "--framerate",
        type=str,
        help="Framerate of encoded video. Default same as input.",
        default="",
    )
    parser.add_argument(
        "-p",
        "--pattern",
        type=str,
        help="Pattern of file names to look for when building a video from frames.  Default %%04d",
        default="",
    )
    parser.add_argument(
        "-ext",
        "--file-extension",
        type=str,
        help="File extention of source data, default is png",
        default="png",
    )
    parser.add_argument(
        "-fn",
        "--filename",
        type=str,
        help="Overwrite default file name of encoded video with <fn>.mp4. Default is <VIDEO_NAME>_encoded.mp4",
        default="",
    )
    parser.add_argument(
        "-ts",
        "--time-scale",
        type=str,
        help="Multiply the encoded video by this value. Greater than 1 slows the video down, less than 1 speeds it up. Default 1.",
        default="",
    )
    parser.add_argument(
        "-w",
        "--width",
        type=str,
        help="Specify width of the encoded video.  If only --width or --height are passed, the other is calculated automatically. "
        "Pass a number like 0.25x or 2x to resize, or a number to specify directly.  Default does not resize.",
        default="",
    )
    parser.add_argument(
        "-ht",
        "--height",
        type=str,
        help="Specify the height of the encoded video.  Same rules apply as the ones oulined for the width",
        default="",
    )
    parser.add_argument(
        "-op",
        "--output-path",
        type=str,
        help="Define output path for encoded video.  Default is the same direcory as source video",
        default="",
    )
    parser.add_argument(
        "-na",
        "--no-audio",
        help="Drop audio stream.  Useful for slowing down high-speed video.",
        action="store_true",
    )
    parser.add_argument(
        "-dr",
        "--dry-run",
        help="Do not encode the video, but print all parameters and final ffmpeg command.",
        action="store_true",
    )

    return parser.parse_args()


def subprocess_call(proc_args):
    p1 = subprocess.Popen(proc_args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    try:
        stdout, stderr = p1.communicate()
    except Exception as stdout:
        stderr = "EXCEPTION"
    return stdout.decode("utf-8"), stderr.decode("utf-8")


def get_video_attributes(source):

    # Input is a video file
    if pathlib.Path(source).is_file():
        video = cv2.VideoCapture(source)
        width, height, fps = (
            int(video.get(cv2.CAP_PROP_FRAME_WIDTH)),
            int(video.get(cv2.CAP_PROP_FRAME_HEIGHT)),
            video.get(cv2.CAP_PROP_FPS),
        )
        is_directory = False

    # Input is a directory
    else:
        files = list(pathlib.Path(source).glob("**/*"))
        try:
            img = cv2.imread(files[0].as_posix())
            height, width, _ = img.shape
            fps = 30
            is_directory = True
        except Exception as e:
            print("##############################################")
            print(e)
            print("##############################################")
            print(f"ERROR: Could not open file {files[0]}")
            print("\nRemove any files from the input directory that are not images.")
            print("##############################################")
            exit(1)

    return width, height, fps, is_directory


def calculate_new_dimensions(args, width, height):

    aspect_ratio = float(width / height)

    if "x" in args.width:
        sf = float * (args.width.strip("x"))
        new_width = width
        new_height = height
    elif "x" in args.height:
        sf = float * (args.height.strip("x"))
        new_width = width
        new_height = height
    else:
        sf = 1
        if args.width and not args.height:
            new_width = int(args.width)
            new_height = new_width / aspect_ratio
        elif args.height and not args.width:
            new_height = int(args.height)
            new_width = new_height * aspect_ratio
        else:
            new_height = height
            new_width = width

    new_width = int(new_width * sf)
    new_height = int(new_height * sf)

    return new_width, new_height


def print_parameters(
    args, width, height, fps, new_width, new_height, new_framerate, output_file
):
    print("INPUT ATTRIBUTES:")
    print(f"Input File: {args.source}")
    print(f"Width: {width}")
    print(f"Height: {height}")
    print(f"Framerate: {fps}\n")
    print(f"OUTPUT ATTRIBUTES:")
    print(f"Output file: {output_file}")
    print(f"Width: {new_width}")
    print(f"Height: {new_height}")
    print(f"Framerate: {new_framerate}")
    audio_string = "None" if args.no_audio else "Copy from input, encode as MP3."
    print(f"Audio: {audio_string}")


def main():

    out, err = subprocess_call(["which", "ffmpeg"])
    if not out:
        print("ERROR: Install ffmpeg before running this script.")
        exit(0)
    args = make_args()
    # Must be manually set, can't set default value in argparser
    if not args.pattern:
        args.pattern = "%04d"
    
    width, height, fps, is_directory = get_video_attributes(args.source)

    new_width, new_height = calculate_new_dimensions(args, width, height)

    new_framerate = int(args.framerate) if args.framerate else fps
    pts = float(args.time_scale) if args.time_scale else 1

    input_path = pathlib.Path(args.source)
    base_file_name = input_path.stem
    output_file_name = (
        args.filename + ".mp4" if args.filename else base_file_name + "_encoded.mp4"
    )
    output_path = (
        pathlib.Path(args.output_path) if args.output_path else input_path.parent
    )
    output_file = output_path / output_file_name

    base_video_filter = f'-filter:v "setpts={pts}*PTS'

    if (new_width % 2 != 0) or (new_height % 2 != 0):
        video_filter = base_video_filter + f',pad=ceil({new_width}/2)*2:ceil({new_height}/2)*2"'
    else:
        video_filter = base_video_filter + f',scale={new_width}:{new_height}"'

    if is_directory:
        source = args.source + args.pattern + f".{args.file_extension}"
    else:
        source = args.source
    if args.no_audio:
        audio_flag = "-an"
    else:
        audio_flag = ""
    cmd = f"ffmpeg -r {new_framerate} -i {source} {video_filter} -preset veryslow -c:v libx264 -c:a mp3 -pix_fmt yuv420p -tune film {audio_flag} {output_file}"

    if args.dry_run:
        print("----------------------------")
        print("-----DRY RUN PARAMETERS-----")
        print("----------------------------")
        print_parameters(
            args, width, height, fps, new_width, new_height, new_framerate, output_file
        )
        print("ffmpeg call:")
        print(cmd)
    else:
        print_parameters(
            args, width, height, fps, new_width, new_height, new_framerate, output_file
        )
        answer = input("Continue [y/n]: ")
        if answer.lower() in ["y", "yes"]:
            print(f'System call: {cmd}')
            os.system(cmd)
        elif answer.lower() in ["n", "no"]:
            exit(0)
        else:
            print("Did not understand input.")
            exit(0)


if __name__ == "__main__":
    main()
